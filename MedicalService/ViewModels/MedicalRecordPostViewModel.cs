﻿namespace MedicalService.ViewModels
{
    public class MedicalRecordPostViewModel
    {
        public int ID { get; set; }
        public string Record { get; set; }
    }
}