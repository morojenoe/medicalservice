﻿using System;

namespace MedicalService.ViewModels
{
    public class AppointmentViewModel
    {
        public DateTime AppointmentDateTime { get; set; }
        public string DoctorId { get; set; }
        public string PatientId { get; set; }
    }
}
