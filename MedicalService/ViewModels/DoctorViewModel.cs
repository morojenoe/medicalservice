﻿using System.Collections.Generic;

namespace MedicalService.ViewModels
{
    public class DoctorViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public bool IsTherePhoto { get; set; }
        public ICollection<string> Specialities { get; set; }
        public ICollection<KeyValuePair<string, string>> WorkPlaces { get; set; }
    }
}
