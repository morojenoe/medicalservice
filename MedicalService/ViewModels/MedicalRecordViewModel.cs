﻿using System;

namespace MedicalService.ViewModels
{
    public class MedicalRecordViewModel
    {
        public int ID { get; set; }
        public string Record { get; set; }
        public DateTime Date { get; set; }
        public DoctorViewModel Doctor { get; set; }
        public PatientViewModel Patient { get; set; }
        public SpecialityViewModel DoctorSpeciality { get; set; }
    }
}
