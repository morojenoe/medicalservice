﻿namespace MedicalService.ViewModels
{
    public class SpecialityViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
