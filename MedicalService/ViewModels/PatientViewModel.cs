﻿namespace MedicalService.ViewModels
{
    public class PatientViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public bool IsTherePhoto { get; set; }
    }
}
