﻿namespace MedicalService.ViewModels
{
    public class PationStatusViewModel
    {
        public bool IsActive { get; set; }
        public PatientViewModel Patient { get; set; }
    }
}
