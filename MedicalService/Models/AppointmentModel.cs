﻿using System;

namespace MedicalService.Models
{
    public class Appointment
    {
        public int ID { get; set; }
        public DateTime AppointmentDateTime { get; set; }
        public virtual ApplicationUser Patient { get; set; }
        public virtual ApplicationUser Doctor { get; set; }
    }
}
