﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MedicalService.Models
{
    public class Hospital
    {
        public int ID { get; set; }

        [Required]
        public string Adress { get; set; }

        [Required]
        public string HospitalName { get; set; }

        public virtual ICollection<ApplicationUser> Doctors { get; set; }
    }
}
