﻿using System.Collections.Generic;

namespace MedicalService.Models
{
    public class Speciality
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public virtual ICollection<ApplicationUser> Doctors { get; set; }
    }
}
