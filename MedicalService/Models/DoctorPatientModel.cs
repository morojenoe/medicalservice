﻿namespace MedicalService.Models
{
    public class DoctorPatientModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string DoctorId { get; set; }
        public string PatientId { get; set; }
        public virtual ApplicationUser Doctor { get; set; }
        public virtual ApplicationUser Patient { get; set; }
    }
}
