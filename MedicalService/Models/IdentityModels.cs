﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace MedicalService.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public bool IsTherePhoto { get; set; }
        public bool IsDoctor { get; set; }
        public virtual ICollection<Speciality> Specialities { get; set; }
        public virtual ICollection<Hospital> WorkPlaces { get; set; }
        public virtual ICollection<DoctorPatientModel> PatientStatus { get; set; }
        public virtual ICollection<DoctorPatientModel> DoctorStatus { get; set; }
    }
}
