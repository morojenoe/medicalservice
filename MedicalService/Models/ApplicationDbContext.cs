using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MedicalService.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public ApplicationDbContext()
            : base("MedicalServiceDB", throwIfV1Schema: false)
        {
        }

        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<MedicalRecord> MedicalRecords { get; set; }
        public DbSet<DoctorPatientModel> DoctorPatient { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<DoctorPatientModel>()
                .HasRequired(m => m.Doctor)
                .WithMany(t => t.PatientStatus)
                .HasForeignKey(m => m.DoctorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DoctorPatientModel>()
                .HasRequired(m => m.Patient)
                .WithMany(t => t.DoctorStatus)
                .HasForeignKey(m => m.PatientId)
                .WillCascadeOnDelete(false);
        }
    }
}