﻿using System;

namespace MedicalService.Models
{
    public class MedicalRecord
    {
        public int ID { get; set; }

        public DateTime Date { get; set; }

        public string Record { get; set; }

        public virtual Speciality DoctorSpeciality { get; set; }

        public virtual ApplicationUser Doctor { get; set; }

        public virtual ApplicationUser Patient { get; set; }
    }
}
