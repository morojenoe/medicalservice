﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MedicalService.Startup))]
namespace MedicalService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
