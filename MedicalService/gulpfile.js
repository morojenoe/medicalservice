/// <binding BeforeBuild='do_all_tasks_without_watch' />
"use strict";

var gulp = require("gulp"),
    less = require("gulp-less"),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify");


var content = "./Content/";
var wwwroot = "./wwwroot/";
var npm_dir = "./node_modules/";
var scripts = "./Scripts/";
var typeScriptConfig = "./tsconfig.json";
var angularDirectory = content + "js/angular/";

var paths = {
    less_src: content + "less/**/*.less",
    less_dest: wwwroot + "css/",
    images_src: content + "images/**/*",
    images_dest: wwwroot + "images/",
    js_src: content + "js/**/*.js",
    js_dest: wwwroot + "js/",
    ts_src: content + "ts/**/*.ts",
    libs_dest: wwwroot + "libs/",
    angularTemplateURLPath: angularDirectory + "path_to_angularjs_files.js",
    angularTemplateHTML_src: angularDirectory + "**/*.html",
    angularTemplateHTML_dest: wwwroot + "html/angular/",
    angular_dest: wwwroot + "js/angular/"
};

// Watch js
gulp.task("watch:js", function () {
    gulp.watch(paths.js_src, ["concat:js"]);
});


// Copy js
gulp.task("copy:js", function () {
    return gulp.src([paths.js_src])
        .pipe(gulp.dest(paths.js_dest));
});

// Concat JS files
function getAngularJSFilesForConcat() {
    var result = [];
    result.push(paths.angularTemplateURLPath);
    result.push(angularDirectory + "**/*.module.js");
    result.push(angularDirectory + "**/*.component.js");
    return result;
}

function getJsFilesForConcat() {
    var result = [];
    result = result.concat(getAngularJSFilesForConcat());
    result.push(paths.js_src);
    return result;
}

gulp.task("concat:js", function () {
    var files = getJsFilesForConcat();
    return gulp.src(files)
        .pipe(concat("app.js"))
        .pipe(gulp.dest(paths.js_dest));
});


// Angular Template HTML copy
gulp.task("copy:angularTemplateHTML", function () {
    return gulp.src([paths.angularTemplateHTML_src])
        .pipe(gulp.dest(paths.angularTemplateHTML_dest));
});

// Copy images
gulp.task("copy:images", function () {
    return gulp.src([paths.images_src])
        .pipe(gulp.dest(paths.images_dest));
});

// Angular Template HTML watch
// Watch ts
gulp.task("watch:angularTemplateHTML", function () {
    gulp.watch(paths.angularTemplateHTML_src, ["copy:angularTemplateHTML"]);
});


// Compile LESS files
gulp.task("compile:less", function () {
    return gulp.src([paths.less_src])
        .pipe(less())
        .pipe(gulp.dest(paths.less_dest));
});

// Watch LESS

gulp.task("watch:less", function () {
    gulp.watch(paths.less_src, ["compile:less"]);
});

gulp.task("watch", ["watch:js", "watch:ts", "watch:angularTemplateHTML", "watch:less"]);
gulp.task("compile", ["compile:less"]);
gulp.task("copy", ["copy:js", "copy:angularTemplateHTML", "copy:images"]);
gulp.task("concat", ["concat:js"]);
gulp.task("do_all_tasks_without_watch", ["compile", "copy", "concat"]);
gulp.task("do_all_tasks", ["do_all_tasks_without_watch", "watch"]);
