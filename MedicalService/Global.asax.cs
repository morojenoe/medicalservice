﻿using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Data.Entity;
using MedicalService.DAL;
using MedicalService.Models;

namespace MedicalService
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            Database.SetInitializer(new ApplicationDbInitializer());
            var appDbContext = new ApplicationDbContext();
            appDbContext.Database.Initialize(false);
        }
    }
}
