﻿angular.module("medicalServiceModule")
    .component("record", {
        templateUrl: pathToAngularJSFiles + "medical_record/medical_record.template.html",
        controller: ["$http", "$routeParams", "Speciality", "DoctorRecords",
            function showRecordController($http, $routeParams, Specialities, DoctorRecords) {
                var vm = this;

                vm.patientId = $routeParams.patientId;
                vm.currentSpecialityId = 1;
                Specialities.query(function(specialities) {
                    vm.specialities = specialities;
                    DoctorRecords.query({patientId: vm.patientId}, function(data)
                    {
                        vm.medicalRecords = {};
                        vm.doctorWithSpeciality = {};
                        var i;
                        for (i = 0; i < vm.specialities.length; i++) {
                            vm.medicalRecords[vm.specialities[i].ID] = {};
                            vm.doctorWithSpeciality[vm.specialities[i].ID] = [];
                        }
                        for (i = 0; i < data.length; i++) {
                            if (vm.doctorWithSpeciality[data[i].DoctorSpeciality.ID].indexOf(data[i].Doctor.ID) === -1)
                                vm.doctorWithSpeciality[data[i].DoctorSpeciality.ID].push(data[i].Doctor.ID);
                            vm.medicalRecords[data[i].DoctorSpeciality.ID][data[i].Doctor.ID] = [];
                        }
                        for (i = 0; i < data.length; i++) {
                            vm.medicalRecords[data[i].DoctorSpeciality.ID][data[i].Doctor.ID].push(data[i]);
                        }
                        if (data.length > 0) {
                            vm.currentPatient = data[0].Patient;
                        }
                    });
                });

                vm.getAllRecords = function (specialityId) {
                    var result = [];
                    for (var i = 0; i < vm.medicalRecords.length; i++) {
                        if (vm.medicalRecords[i].DoctorSpeicality.ID === specialityId) {
                            result.push(vm.medicalRecords[i]);
                        }
                    }
                    return result;
                };

                vm.doctorPresentation = function (doctor) {
                    return doctor.Surname + " " + doctor.Name[0] + "." + (doctor.Patronymic === null ? "" : doctor.Patronymic[0] + ".");
                };

                vm.dateInReadableFormat = function(date) {
                    var year = date.slice(0, 4);
                    var month = date.slice(5, 7);
                    var day = date.slice(8, 10);

                    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    return day + " " + months[parseInt(month) - 1] + " " + year;
                };

                vm.saveChanges = function(recordId, record) {
                    $http.put("/api/DoctorRecords?recordId=" + recordId, { "ID": recordId, "Record": record });
                };
            }]
    });
