﻿angular.
  module('medicalServiceModule').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.when('/',
            {
                template: '<show-patients></show-patients>'
            })
            .when('/:patientId',
            {
                template: '<record></record>'
            });
    }]);
