﻿angular.module("medicalServiceModule")
    .component("doctorsAppointment", {
        templateUrl: pathToAngularJSFiles + "doctors_appointment/doctors_appointment.template.html",
        controller: ["$http", "$uibModal", "Appointments", "moment",
            function showDoctorsController($http, $uibModal, Appointments, moment) {
                var vm = this;

                vm.selectedDay = new Date().getDate();

                $http.get("/api/doctors").then(function (response) {
                    vm.doctors = response.data;
                    vm.selectedDoctor = vm.doctors[0];
                    vm.selectDoctor(vm.doctors[0]);
                });

                vm.getDays = function () {
                    if (vm.days !== undefined)
                        return vm.days;
                    vm.days = [];
                    var date = moment();
                    for (var d = 0; d < 7; d++) {
                        vm.days.push(date.clone());
                        date = date.add(1, 'days');
                    }
                    return vm.days;
                };
                
                vm.appointmentIsFree = function (day, hours, minutes) {
                    if (vm.appointments === undefined) {
                        return true;
                    }
                    var appointment = (day.date() < 10? "0" + day.date() : day.date()) + "T" + hours + ":" + minutes;
                    return !vm.appointments.has(appointment);
                };

                vm.selectDoctor = function (doctor) {
                    Appointments.query({ 'doctorId': doctor.ID },
                        function(appointmentsData) {
                            vm.appointments = new Set();
                            for (var i = 0; i < appointmentsData.length; i++) {
                                var data = appointmentsData[i];
                                var dateTime = data["AppointmentDateTime"].slice(8, -3);
                                vm.appointments.add(dateTime);
                            }
                            vm.selectedDoctor = doctor;
                        });
                };

                vm.openModal = function(hour, min, day) {
                    var modalInstance = $uibModal.open({
                        component: 'setAppointment',
                        resolve: {
                            hour: function() {
                                return hour;
                            },
                            min: function() {
                                return min;
                            },
                            day: function () {
                                return day;
                            },
                            doctor: function () {
                                return vm.selectedDoctor;
                            }
                        }
                    });

                    modalInstance.result.then(function() {
                        var appointment = (day.date() < 10 ? "0" + day.date() : day.date()) + "T" + hour + ":" + min;
                        vm.appointments.add(appointment);
                        var cloneDay = day.clone();
                        cloneDay.minute(min);
                        cloneDay.hour(hour);
                        Appointments.save({ 'DoctorId': vm.selectedDoctor.ID, 'AppointmentDateTime': cloneDay.format("YYYY-MM-DD HH:mm") });
                    });
                };
            }]
    });
