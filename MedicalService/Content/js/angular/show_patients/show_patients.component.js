﻿angular.module("medicalServiceModule")
    .component("showPatients", {
    	templateUrl: pathToAngularJSFiles + "show_patients/show_patients.template.html",
    	controller: ['PatientStatus',
            function showPatientsController(PatientStatus) {
                var vm = this;
                vm.patients = { 0: [], 1: [] };
                PatientStatus.query(function(patientStatus) {
                    for (var i = 0; i < patientStatus.length; i++) {
                        var id = patientStatus[i].IsActive ? 0 : 1;
                        vm.patients[id].push(patientStatus[i].Patient);
                    } 
                });
            }]
    });
