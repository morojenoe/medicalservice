﻿angular.module("medicalServiceModule")
    .component("about", {
        templateUrl: pathToAngularJSFiles + "about/about.template.html",
        controller: [function() {
            var vm = this;
            vm.slides = [
                {
                    'image': '/wwwroot/images/about/1.jpg',
                    'caption': 'Database of doctors',
                    'text': 'Easily find an appropriate doctor'
                },
                {
                    'image': '/wwwroot/images/about/2.jpg',
                    'caption': 'Doctor\'s appointment',
                    'text': 'Set an appointment with doctor in two clicks'
                },
                {
                    'image': '/wwwroot/images/about/3.jpg',
                    'caption': 'Online medical record',
                    'text': 'Fast search and convenient filling of medical cards'
                }];
        }]
    });
