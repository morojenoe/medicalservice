﻿angular.module("medicalServiceModule")
    .factory("DoctorRecords", ["$resource",
        function ($resource) {
            return $resource("/api/DoctorRecords?patientId=:patientId");
        }]);
