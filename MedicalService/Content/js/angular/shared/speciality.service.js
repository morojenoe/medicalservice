﻿angular.module("medicalServiceModule")
    .factory("Speciality", ["$resource",
        function($resource) {
            return $resource("/api/specialities/");
        }]);
