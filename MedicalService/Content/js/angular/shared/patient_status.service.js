﻿angular.module("medicalServiceModule")
    .factory("PatientStatus", ["$resource",
        function ($resource) {
            return $resource("/api/PatientStatus/");
        }]);
