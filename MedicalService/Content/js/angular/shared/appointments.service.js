﻿angular.module("medicalServiceModule")
    .factory("Appointments", ["$resource",
        function ($resource) {
            return $resource("/api/Appointments?doctorID=:doctorId");
        }]);
