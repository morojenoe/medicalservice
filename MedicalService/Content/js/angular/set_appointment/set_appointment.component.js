﻿angular.module('medicalServiceModule')
    .component('setAppointment', {
        templateUrl: pathToAngularJSFiles + "set_appointment/set_appointment.template.html",
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        },
        controller: [function() {
            var vm = this;

            vm.$onInit = function() {
                vm.doctor = vm.resolve.doctor;
                vm.hour = vm.resolve.hour;
                vm.min = vm.resolve.min;
                vm.day = vm.resolve.day;
            };

            vm.ok = function() {
                vm.close();
            };

            vm.cancel = function() {
                vm.dismiss();
            }
        }]
    });
