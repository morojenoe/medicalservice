﻿using System;
using MedicalService.Models;
using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using WebGrease.Css.Extensions;

namespace MedicalService.DAL
{
    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        private const string DoctorRole = "doctor";
        private const string PatientRole = "patient";

        protected override void Seed(ApplicationDbContext context)
        {
            base.Seed(context);
            AddRoles(context);
            AddSpecialities(context);
            AddHospitals(context);
            AddUsers(context);
            AddAppointments(context);
            AddMedicalRecords(context);
            AddPatientStatus(context);
            context.SaveChanges();
        }

        private void AddPatientStatus(ApplicationDbContext context)
        {
            var patientStatus = new[]
            {
                new DoctorPatientModel()
                {
                    IsActive = true,
                    Doctor = context.Users.Find("1"),
                    Patient = context.Users.Find("7"),
                },
                new DoctorPatientModel()
                {
                    IsActive = false,
                    Doctor = context.Users.Find("1"),
                    Patient = context.Users.Find("8"),
                },
                new DoctorPatientModel()
                {
                    IsActive = true,
                    Doctor = context.Users.Find("1"),
                    Patient = context.Users.Find("9"),
                },
            };

            context.DoctorPatient.AddRange(patientStatus);
            context.SaveChanges();
        }

        private void AddMedicalRecords(ApplicationDbContext context)
        {
            var records = new[]
            {
                new MedicalRecord()
                {
                    ID = 1,
                    Record = "The patient have wheezing in the chest. Sended on analysis of blood.",
                    Date = DateTime.Now,
                    DoctorSpeciality = context.Specialities.Find(1),
                    Patient = context.Users.Find("7"),
                    Doctor = context.Users.Find("1"),
                },
                new MedicalRecord()
                {
                    ID = 2,
                    Record = "I don't know what with him. Sended to the surgeon.",
                    Date = DateTime.Now,
                    DoctorSpeciality = context.Specialities.Find(1),
                    Patient = context.Users.Find("8"),
                    Doctor = context.Users.Find("2"),
                },
                new MedicalRecord()
                {
                    ID = 3,
                    Record = "The patient is healthy.",
                    Date = DateTime.Now,
                    DoctorSpeciality = context.Specialities.Find(1),
                    Patient = context.Users.Find("8"),
                    Doctor = context.Users.Find("3"),
                },
                new MedicalRecord()
                {
                    ID = 4,
                    Record = "The blood test is failed.",
                    Date = DateTime.Now,
                    DoctorSpeciality = context.Specialities.Find(1),
                    Patient = context.Users.Find("7"),
                    Doctor = context.Users.Find("3"),
                },
                new MedicalRecord()
                {
                    ID = 5,
                    Record = "The patient didn't come.",
                    Date = DateTime.Now,
                    DoctorSpeciality = context.Specialities.Find(2),
                    Patient = context.Users.Find("8"),
                    Doctor = context.Users.Find("4"),
                },
                new MedicalRecord()
                {
                    ID = 6,
                    Record = "Some record.",
                    Date = DateTime.Now,
                    DoctorSpeciality = context.Specialities.Find(5),
                    Patient = context.Users.Find("7"),
                    Doctor = context.Users.Find("4"),
                },
                new MedicalRecord()
                {
                    ID = 7,
                    Record = "Analysis of blood is good. Patient is healthy.",
                    Date = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)),
                    DoctorSpeciality = context.Specialities.Find(1),
                    Patient = context.Users.Find("7"),
                    Doctor = context.Users.Find("1"),
                },
            };

            records.ForEach(record => context.MedicalRecords.Add(record));
            context.SaveChanges();
        }

        private void AddRoles(ApplicationDbContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists(DoctorRole))
            {
                var roleResult = roleManager.Create(new IdentityRole(DoctorRole));
            }
            if (!roleManager.RoleExists(PatientRole))
            {
                var roleResult = roleManager.Create(new IdentityRole(PatientRole));
            }
        }

        private void AddUsers(ApplicationDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var doctors = new[]
            {
                new ApplicationUser()
                {
                    Id = "1",
                    Name = "Pavel",
                    Surname = "Braslavtsev",
                    Patronymic = "Vasilievich",
                    IsTherePhoto = true,
                    Specialities = new List<Speciality>()
                    {
                        context.Specialities.Find(1)
                    },
                    IsDoctor = true,
                    Email = "pavel@yandex.ru",
                    UserName = "pavel@yandex.ru",
                    PhoneNumber = "+79641824876",
                    WorkPlaces = new List<Hospital>()
                    {
                        context.Hospitals.Find(1),
                        context.Hospitals.Find(2),
                    },
                },

                new ApplicationUser()
                {
                    Id = "2",
                    Name = "Sergey",
                    Surname = "Nepogoda",
                    Patronymic = "Vitalievich",
                    IsTherePhoto = true,
                    Specialities = new List<Speciality>()
                    {
                        context.Specialities.Find(2),
                        context.Specialities.Find(4),
                    },
                    IsDoctor = true,
                    Email = "sergey@avicena.ru",
                    UserName = "sergey@avicena.ru",
                    PhoneNumber = "+79123314022",
                    WorkPlaces = new List<Hospital>()
                    {
                        context.Hospitals.Find(1),
                    },
                },

                new ApplicationUser()
                {
                    Id = "3",
                    Name = "Dmitry",
                    Surname = "Gubin",
                    Patronymic = "Andreevich",
                    IsTherePhoto = true,
                    Specialities = new List<Speciality>()
                    {
                        context.Specialities.Find(1),
                        context.Specialities.Find(3),
                        context.Specialities.Find(5),
                    },
                    IsDoctor = true,
                    Email = "dbugin@doctorplus.ru",
                    UserName = "dbugin@doctorplus.ru",
                    PhoneNumber = "+79441114033",
                    WorkPlaces = new List<Hospital>()
                    {
                        context.Hospitals.Find(2),
                    },
                },

                new ApplicationUser()
                {
                    Id = "4",
                    Name = "Anatoliy",
                    Surname = "Tsarev",
                    Patronymic = "Markovich",
                    IsTherePhoto = true,
                    Specialities = new List<Speciality>()
                    {
                        context.Specialities.Find(4),
                    },
                    IsDoctor = true,
                    Email = "atsarev@avicena.ru",
                    UserName = "atsarev@avicena.ru",
                    PhoneNumber = "+79005553535",
                    WorkPlaces = new List<Hospital>()
                    {
                        context.Hospitals.Find(2),
                        context.Hospitals.Find(3),
                    },
                },

                new ApplicationUser()
                {
                    Id = "5",
                    Name = "Afanasiy",
                    Surname = "Fet",
                    Patronymic = "Sergeevich",
                    IsTherePhoto = true,
                    Specialities = new List<Speciality>()
                    {
                        context.Specialities.Find(3),
                        context.Specialities.Find(5),
                    },
                    IsDoctor = true,
                    Email = "afet@alanclinic.ru",
                    UserName = "afet@alanclinic.ru",
                    PhoneNumber = "+79092193030",
                    WorkPlaces = new List<Hospital>()
                    {
                        context.Hospitals.Find(1),
                        context.Hospitals.Find(3),
                    },
                },

                new ApplicationUser()
                {
                    Id = "6",
                    Name = "TestDoctorName",
                    Surname = "TestDoctorSurname",
                    Patronymic = "TestDoctorPatronymic",
                    IsTherePhoto = true,
                    Specialities = new List<Speciality>()
                    {
                        context.Specialities.Find(1),
                        context.Specialities.Find(5),
                    },
                    IsDoctor = true,
                    Email = "test_doctor@gmail.com",
                    UserName = "test_doctor@gmail.com",
                    PhoneNumber = "+79092193030",
                    WorkPlaces = new List<Hospital>()
                    {
                        context.Hospitals.Find(1),
                    },
                },
            };

            var patients = new[]
            {
                new ApplicationUser()
                {
                    Id = "7",
                    Name = "Alexander",
                    Surname = "Pushkin",
                    Patronymic = "Sergeevich",
                    IsTherePhoto = true,
                    IsDoctor = false,
                    Email = "apushkin@gmail.com",
                    UserName = "apushkin@gmail.com",
                    PhoneNumber = "+79093812019",
                },

                new ApplicationUser()
                {
                    Id = "8",
                    Name = "TestPatientName",
                    Surname = "TestPatientSurname",
                    Patronymic = "TestPatientPatronimic",
                    IsTherePhoto = false,
                    IsDoctor = false,
                    Email = "test_patient@gmail.com",
                    UserName = "test_patient@gmail.com",
                    PhoneNumber = "+79093812019",
                },

                new ApplicationUser()
                {
                    Id = "9",
                    Name = "Arnold",
                    Surname = "Schwarzenegger",
                    Patronymic = null,
                    IsTherePhoto = true,
                    IsDoctor = false,
                    Email = "schwarzenegger@gmail.com",
                    UserName = "schwarzenegger@gmail.com",
                    PhoneNumber = "1-844-271-5536",
                },
            };

            const string password = "123456Aa!";

            doctors.ForEach(doctor => userManager.Create(doctor, password));
            patients.ForEach(patient => userManager.Create(patient, password));

            doctors.ForEach(doctor => userManager.AddToRole(doctor.Id, DoctorRole));
            patients.ForEach(patient => userManager.AddToRole(patient.Id, PatientRole));
            context.SaveChanges();
        }

        private void AddHospitals(ApplicationDbContext context)
        {
            context.Hospitals.Add(new Hospital()
            {
                HospitalName = "Doctor+",
                Adress = "Petrova st., 9",
                Doctors = null,
            });

            context.Hospitals.Add(new Hospital()
            {
                HospitalName = "Avicena",
                Adress = "Molodezhnaya st., 68a",
                Doctors = null,
            });

            context.Hospitals.Add(new Hospital()
            {
                HospitalName = "Alan Clinic",
                Adress = "Lenina st., 12",
                Doctors = null,
            });

            context.SaveChanges();
        }

        private void AddAppointments(ApplicationDbContext context)
        {
            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0),
                Doctor = context.Users.Find("1"),
                Patient = context.Users.Find("6"),
            });

            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 10, 0),
                Doctor = context.Users.Find("2"),
                Patient = context.Users.Find("6"),
            });

            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 0, 0),
                Doctor = context.Users.Find("2"),
                Patient = context.Users.Find("6"),
            });

            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0),
                Doctor = context.Users.Find("4"),
                Patient = context.Users.Find("6"),
            });
            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 10, 0),
                Doctor = context.Users.Find("4"),
                Patient = context.Users.Find("6"),
            });
            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 30, 0),
                Doctor = context.Users.Find("4"),
                Patient = context.Users.Find("6"),
            });
            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 10, 0),
                Doctor = context.Users.Find("5"),
                Patient = context.Users.Find("6"),
            });
            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 00, 0),
                Doctor = context.Users.Find("5"),
                Patient = context.Users.Find("6"),
            });
            context.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 30, 0),
                Doctor = context.Users.Find("5"),
                Patient = context.Users.Find("6"),
            });

            context.SaveChanges();
        }

        private void AddSpecialities(ApplicationDbContext context)
        {
            var specialities = new[] {
                "Surgeon", "Resuscitator", "Therapeutist", "Neurologist", "Psychiatrist"
            };
            foreach (var speciality in specialities)
            {
                context.Specialities.Add(new Speciality() { Name = speciality });
            }
            context.SaveChanges();
        }
    }
}
