﻿using System.Web.Mvc;

namespace MedicalService.Controllers
{
    public class PatientController : Controller
    {
        [AllowAnonymous]
        // GET: DoctorsAppointment
        public ActionResult Appointment()
        {
            return View();
        }
    }
}
