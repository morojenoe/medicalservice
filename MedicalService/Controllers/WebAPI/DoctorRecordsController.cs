﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using MedicalService.Models;
using MedicalService.ViewModels;
using Microsoft.AspNet.Identity;

namespace MedicalService.Controllers.WebAPI
{
    public class DoctorRecordsController : ApiController
    {
        private readonly ApplicationDbContext _appContext = new ApplicationDbContext();

        public IEnumerable<MedicalRecordViewModel> GetDoctorRecords(string patientId)
        {
            return _appContext.MedicalRecords.ToList()
                .Where(record => record.Patient.Id == patientId)
                .Select(record => new MedicalRecordViewModel()
                {
                    ID = record.ID,
                    Date = record.Date,
                    Record = record.Record,
                    Doctor = new DoctorViewModel()
                    {
                        ID = record.Doctor.Id,
                        Name = record.Doctor.Name,
                        Surname = record.Doctor.Surname,
                        Patronymic = record.Doctor.Patronymic,
                        IsTherePhoto = record.Doctor.IsTherePhoto,
                        Specialities = record.Doctor.Specialities.Select(spec => spec.Name).ToArray(),
                    },
                    Patient = new PatientViewModel()
                    {
                        ID = record.Patient.Id,
                        Name = record.Patient.Name,
                        Surname = record.Patient.Surname,
                        Patronymic = record.Patient.Patronymic,
                        IsTherePhoto = record.Patient.IsTherePhoto,
                    },
                    DoctorSpeciality = new SpecialityViewModel()
                    {
                        ID = record.DoctorSpeciality.ID,
                        Name = record.DoctorSpeciality.Name,
                    },
                })
                .ToArray();
        }

        public async Task<IHttpActionResult> PutUpdateRecord(int recordId, [FromBody]MedicalRecordPostViewModel record)
        {
            if (!ModelState.IsValid || recordId != record.ID)
            {
                return BadRequest(ModelState);
            }
            var doctorId = User.Identity.GetUserId();
            var medicalRecord = await _appContext.MedicalRecords.FindAsync(recordId);
            if (medicalRecord == null || medicalRecord.Doctor.Id != doctorId)
            {
                return BadRequest();
            }
            
            medicalRecord.Record = record.Record;
            _appContext.Entry(medicalRecord).State = EntityState.Modified;
            return Ok(_appContext.SaveChangesAsync());
        }
    }
}
