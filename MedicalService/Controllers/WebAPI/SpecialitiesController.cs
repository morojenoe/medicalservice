﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MedicalService.Models;
using MedicalService.ViewModels;

namespace MedicalService.Controllers.WebAPI
{
    public class SpecialitiesController : ApiController
    {
        private readonly ApplicationDbContext _appContext = new ApplicationDbContext();

        public IEnumerable<SpecialityViewModel> GetSpecialities()
        {
            return _appContext.Specialities.ToList()
                .Select(spec => new SpecialityViewModel()
                {
                    ID = spec.ID,
                    Name = spec.Name,
                })
                .ToArray();
        }
    }
}
