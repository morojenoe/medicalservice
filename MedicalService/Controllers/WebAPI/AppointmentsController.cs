﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using MedicalService.Models;
using MedicalService.ViewModels;
using Microsoft.AspNet.Identity;


namespace MedicalService.Controllers.WebAPI
{
    public class AppointmentsController : ApiController
    {
        private readonly ApplicationDbContext _appContext = new ApplicationDbContext();

        public IEnumerable<AppointmentViewModel> GetAppointments(string doctorID)
        {
            return _appContext.Appointments.ToList()
                .Where(appointment => appointment.Doctor.Id == doctorID)
                .Select(appointment => new AppointmentViewModel() { AppointmentDateTime = appointment.AppointmentDateTime })
                .ToArray();
        }

        [HttpPost]
        public async Task<IHttpActionResult> PostAddAppointment(AppointmentViewModel appointment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var patientId = User.Identity.GetUserId();
            if (patientId == null)
            {
                return BadRequest();
            }
            appointment.PatientId = patientId;
            if (_appContext.Appointments.ToList()
                .Where(app => app.Doctor.Id == appointment.DoctorId)
                .Any(app => app.AppointmentDateTime == appointment.AppointmentDateTime))
            {
                return BadRequest();
            }
            _appContext.Appointments.Add(new Appointment()
            {
                AppointmentDateTime = appointment.AppointmentDateTime,
                Doctor = _appContext.Users.Find(appointment.DoctorId),
                Patient = _appContext.Users.Find(appointment.PatientId),
            });
            await Task.CompletedTask;
            return Ok(_appContext.SaveChangesAsync());
        }
    }
}
