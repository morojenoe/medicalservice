﻿using System.Collections.Generic;
using System.Web.Http;
using MedicalService.Models;
using System.Linq;
using MedicalService.ViewModels;

namespace MedicalService.Controllers.WebAPI
{
    public class DoctorsController : ApiController
    {
        private readonly ApplicationDbContext _appContext = new ApplicationDbContext();
        public IEnumerable<DoctorViewModel> GetDoctors()
        {
            return _appContext.Users.ToList()
                .Where(user => user.IsDoctor)
                .Select(doctor => new DoctorViewModel() {
                    ID = doctor.Id,
                    Name = doctor.Name,
                    Surname = doctor.Surname,
                    Patronymic = doctor.Patronymic,
                    IsTherePhoto = doctor.IsTherePhoto,
                    Specialities = doctor.Specialities.Select(speciality => speciality.Name).ToArray(),
                    WorkPlaces = doctor.WorkPlaces.Select(workplace => new KeyValuePair<string, string>(workplace.HospitalName, workplace.Adress)).ToArray(),
                })
                .ToArray();
        }
    }
}
