﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MedicalService.Models;
using MedicalService.ViewModels;
using Microsoft.AspNet.Identity;

namespace MedicalService.Controllers.WebAPI
{
    public class PatientStatusController : ApiController
    {
        public ApplicationDbContext _appContext = new ApplicationDbContext();
        public IEnumerable<PationStatusViewModel> GetPatients()
        {
            var doctorId = User.Identity.GetUserId();
            if (doctorId == null)
                return new List<PationStatusViewModel>();
            return _appContext.Users.Find(doctorId).PatientStatus
                .ToList()
                .Select(patient => new PationStatusViewModel()
                {
                    IsActive = patient.IsActive,
                    Patient = new PatientViewModel()
                    {
                        ID = patient.Patient.Id,
                        Name = patient.Patient.Name,
                        Surname = patient.Patient.Surname,
                        Patronymic = patient.Patient.Patronymic,
                        IsTherePhoto = patient.Patient.IsTherePhoto,
                    },
                })
                .ToArray();
        }
    }
}
