﻿using System.Security.Principal;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MedicalService.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetName(this IIdentity identity)
        {
            var user = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(identity.GetUserId());
            return (user == null) ? string.Empty : user.Name;
        }
        public static string GetSurname(this IIdentity identity)
        {
            var user = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(identity.GetUserId());
            return (user == null) ? string.Empty : user.Surname;
        }
    }
}
